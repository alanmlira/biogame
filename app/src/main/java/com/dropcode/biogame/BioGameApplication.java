package com.dropcode.biogame;

import android.app.Application;

import com.dropcode.biogame.infrastructure.di.component.ApplicationComponent;
import com.dropcode.biogame.infrastructure.di.component.DaggerApplicationComponent;
import com.dropcode.biogame.infrastructure.di.module.ApplicationModule;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;

import uk.co.chrisjenx.calligraphy.CalligraphyConfig;

/**
 * Created by AlanmLira on 10/06/16.
 */
public class BioGameApplication extends Application {
    private ApplicationComponent applicationComponent;

    private static BioGameApplication instance;

    @Override
    public void onCreate() {
        super.onCreate();
        instance = this;

        applicationComponent = DaggerApplicationComponent.builder()
                .applicationModule(new ApplicationModule(this))
                .build();

        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);

        CalligraphyConfig.initDefault(new CalligraphyConfig.Builder()
                                .setDefaultFontPath("fonts/Comfortaa-Regular.ttf")
                                .setFontAttrId(R.attr.fontPath)
                                .build());
    }

    public static BioGameApplication instance(){
        return instance;
    }

    public ApplicationComponent getApplicationComponent() {
        return applicationComponent;
    }
}
