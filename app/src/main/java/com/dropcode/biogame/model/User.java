package com.dropcode.biogame.model;

import com.facebook.AccessToken;
import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

/**
 * Created by AlanmLira on 10/06/16.
 */
public class User implements Serializable {

    @SerializedName("id") public int id = 0;
    @SerializedName("name") public String name;
    @SerializedName("email") public String email;
    @SerializedName("fb_access_token") public String fbAccessToken;
    @SerializedName("high_score") public int highScore;
    @SerializedName("last_score") public int lastScore;
    @SerializedName("max_hits") public int maxHits;
    @SerializedName("photo_url") public String photoUrl;

    public User()
    {

    }


    public User(int id, String name, String email, String fbAccessToken, String photoUrl) {
        this.id = id;
        this.name = name;
        this.email = email;
        this.fbAccessToken = fbAccessToken;
        this.photoUrl = photoUrl;
    }

    public Boolean isLogado()
    {
        return AccessToken.getCurrentAccessToken() != null;
    }

}
