package com.dropcode.biogame.model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by AlanmLira on 10/06/16.
 */
public class Question {

    @SerializedName("category") public String category;
    @SerializedName("question") public String question;
    @SerializedName("all_answers") public List<String> allAnswers;
    @SerializedName("right_answer") public String rightAnswer;

    public Question(String category, String question, List<String> allAnswers, String rightAnswer) {
        this.category = category;
        this.question = question;
        this.allAnswers = allAnswers;
        this.rightAnswer = rightAnswer;
    }
}
