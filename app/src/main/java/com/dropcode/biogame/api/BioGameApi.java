package com.dropcode.biogame.api;

import com.dropcode.biogame.model.Question;

import java.util.List;

import retrofit2.http.GET;
import rx.Observable;

/**
 * Created by AlanmLira on 12/06/16.
 */
public interface BioGameApi {

    @GET("/u/36336684/questions_evolucao/")
    Observable<List<Question>> requestEvolutionQuests();
    @GET("/u/36336684/questions_genetica/")
    Observable<List<Question>> requestGeneticQuests();
    @GET("/u/36336684/questions_microbiologia/")
    Observable<List<Question>> requestMicrobiologyQuests();
    @GET("/u/36336684/questions_parasitologia/")
    Observable<List<Question>> requestParasitologyQuests();
    @GET("/u/36336684/questions_zoologia/")
    Observable<List<Question>> requestZoologyQuests();
    @GET("/u/36336684/questions_all/")
    Observable<List<Question>> requestAllQuests();
}
