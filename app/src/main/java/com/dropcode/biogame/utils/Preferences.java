package com.dropcode.biogame.utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.dropcode.biogame.model.User;

/**
 * Created by alan on 6/14/16.
 */
public class Preferences {

    public static void saveUserInPreferences(Context ctx, User user) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences("user", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.putInt("id", user.id);
        editor.putString("name", user.name);
        editor.putString("email", user.email);
        editor.putString("fb_access_token", user.fbAccessToken);
        editor.putInt("high_score", user.highScore);
        editor.putInt("last_score", user.lastScore);
        editor.putInt("max_hits", user.maxHits);
        editor.putString("photo", user.photoUrl);
        editor.apply();
    }

    public static void deleteUserInPreferences(Context ctx) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences("user", Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = sharedPreferences.edit();
        editor.clear().apply();
    }

    public static User getUserInPreferences(Context ctx) {
        SharedPreferences sharedPreferences = ctx.getSharedPreferences("user", Context.MODE_PRIVATE);

        User user = new User();
        user.id = sharedPreferences.getInt("id", 0);
        user.name = sharedPreferences.getString("name", "");
        user.email = sharedPreferences.getString("email", "");
        user.fbAccessToken = sharedPreferences.getString("fb_access_token", "");
        user.photoUrl = sharedPreferences.getString("photo", "");
        user.highScore = sharedPreferences.getInt("high_score", 0);
        user.lastScore = sharedPreferences.getInt("last_score", 0);
        user.maxHits = sharedPreferences.getInt("max_hits", 0);

        return user;
    }
}
