package com.dropcode.biogame.view.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.dropcode.biogame.R;

/**
 * Created by AlanmLira on 14/06/16.
 */
public class ViewPagerFragment extends Fragment {

    private View view;
    private TextView txtTutorial;
    public int position;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        super.onCreateView(inflater, container, savedInstanceState);

        view = inflater.inflate(R.layout.fragment_view_pager, container, false);
        showText();
        return view;
    }

    public void showText(){
        txtTutorial = (TextView)view.findViewById(R.id.txt_desc);
        String msg = "";
        switch (position){
            case 0:
                msg = getContext().getString(R.string.slide1);
                break;
            case 1:
                msg = getContext().getString(R.string.slide2);
                break;
            case 2:
                msg = getContext().getString(R.string.slide3);
                break;
            case 3:
                msg = getContext().getString(R.string.slide4);
                break;
            case 4:
                msg = getContext().getString(R.string.slide5);
                break;

        }

        txtTutorial.setText(msg);
    }
}