package com.dropcode.biogame.view.activities;

import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.support.v4.view.ViewPager;
import android.util.Base64;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;

import com.badoualy.stepperindicator.StepperIndicator;
import com.bumptech.glide.Glide;
import com.dropcode.biogame.R;
import com.dropcode.biogame.utils.Preferences;
import com.dropcode.biogame.view.adapters.ViewPagerAdapter;
import com.facebook.FacebookSdk;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

import bolts.AppLinks;
import butterknife.BindView;
import butterknife.ButterKnife;
import cn.trinea.android.view.autoscrollviewpager.AutoScrollViewPager;

public class MainActivity extends BaseActivity {
    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.vp_slide) AutoScrollViewPager mPager;
    @BindView(R.id.indicator) StepperIndicator mIndicator;
    @BindView(R.id.img_bg) ImageView imageBg;
    @BindView(R.id.ll_buttons) LinearLayout llButons;

    private PagerAdapter mPagerAdapter;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        FacebookSdk.sdkInitialize(getApplicationContext());
        Uri targetUrl = AppLinks.getTargetUrlFromInboundIntent(this, getIntent());
        if (targetUrl != null) {
            Log.i("Activity", "App Link Target URL: " + targetUrl.toString());
        }

        printKeyHash();
    }

    @Override
    protected void onResume() {
        super.onResume();
        setContentView(R.layout.activity_main);
        getApplicationComponent().inject(this);

        ButterKnife.bind(this);

        Glide.with(this).load(R.drawable.slide_bg1).into(imageBg);

        if(Preferences.getUserInPreferences(this).isLogado()) {
            callHome();
        }

        // Instantiate a ViewPager and a PagerAdapter.
        mPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        mPager.setAdapter(mPagerAdapter);

        mIndicator.setViewPager(mPager);

        mPager.setOnPageChangeListener(new ViewPager.OnPageChangeListener() {
            @Override
            public void onPageScrolled(int position, float positionOffset, int positionOffsetPixels) {

            }

            @Override
            public void onPageSelected(int position) {
                switch (position){
                    case 1:
                        Glide.with(MainActivity.this)
                                .load(R.drawable.slide_bg2)
                                .crossFade()
                                .into(imageBg);
                        break;
                    case 2:
                        Glide.with(MainActivity.this)
                                .load(R.drawable.slide_bg3)
                                .crossFade()
                                .into(imageBg);
                        break;
                    case 3:
                        Glide.with(MainActivity.this)
                                .load(R.drawable.slide_bg5)
                                .crossFade()
                                .into(imageBg);
                        break;
                    case 4:
                        Glide.with(MainActivity.this)
                                .load(R.drawable.slide_bg6)
                                .crossFade()
                                .into(imageBg);
                        break;
                    default:
                        Glide.with(MainActivity.this)
                                .load(R.drawable.slide_bg1)
                                .crossFade()
                                .into(imageBg);
                        break;

                }
            }

            @Override
            public void onPageScrollStateChanged(int state) {

            }
        });

        mPager.startAutoScroll(6000);
        mPager.setInterval(6000);
    }

    @Override
    protected void onStop() {
        super.onStop();
        mPager.stopAutoScroll();
    }

    private void printKeyHash() {
        // Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo("com.dropcode.biogame", PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
            }
        } catch (PackageManager.NameNotFoundException e) {
            Log.e("KeyHash:", e.toString());
        } catch (NoSuchAlgorithmException e) {
            Log.e("KeyHash:", e.toString());
        }
    }


    private void callHome() {
        Intent home = new Intent(this, HomeActivity.class);
        startActivity(home);

        if(Preferences.getUserInPreferences(this).isLogado()) {
            finish();
        }
    }

    public void onClickJogar(View view){
        callHome();
    }
}
