package com.dropcode.biogame.view.activities;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.dropcode.biogame.R;
import com.facebook.CallbackManager;
import com.facebook.FacebookSdk;
import com.facebook.share.model.AppInviteContent;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.widget.AppInviteDialog;
import com.facebook.share.widget.ShareDialog;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by alan on 6/15/16.
 */
public class CongratulationsActivity extends BaseActivity {

    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.tx_acuracy_value)
    TextView txAccuracyValue;
    @BindView(R.id.tx_hits_value)
    TextView txHitsValue;
    @BindView(R.id.tx_score_value)
    TextView txScoreValue;
    @BindView(R.id.tx_congrats)
    TextView txCongrats;

    @BindView(R.id.btn_sair)
    Button btnSair;
    @BindView(R.id.btn_categorias)
    Button btnCategorias;
    @BindView(R.id.btn_invite)
    Button btnInvite;
    @BindView(R.id.btn_share)
    Button btnShare;

    CallbackManager callbackManager;
    ShareDialog shareDialog;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_congratulations);

        ButterKnife.bind(this);

        FacebookSdk.sdkInitialize(getApplicationContext());
        callbackManager = CallbackManager.Factory.create();
        shareDialog = new ShareDialog(this);

        String score = getIntent().getStringExtra("score");
        String accuracy = getIntent().getStringExtra("accuracy");
        String hits = getIntent().getStringExtra("hits");
        String categoria = getIntent().getStringExtra("category");

        initializeValues(accuracy, hits, score, categoria);
    }

    public void initializeValues(String accuracyValue, String hitsValue, String scoreValue, String categoria)
    {
        String accuracy = accuracyValue + "%";
        txAccuracyValue.setText(accuracy);
        txHitsValue.setText(hitsValue);
        txScoreValue.setText(scoreValue);

        int ac = Integer.parseInt(accuracyValue);

        if (ac <= 20)
        {
            txCongrats.setText("Estude mais e tente novamente, você consegue! :)");
        }  else
        if (ac <= 50)
        {
            txCongrats.setText("Quase lá! \n Mais atenção da próxima vez, você consegue! :)");

        } else
        if (ac <= 80){
            txCongrats.setText("Parabéns! \n Você realmente entende do assunto! \n " +
                    "Compartilhe essa conquista no Facebook.");
        } else {
            txCongrats.setText("Excelente! \n Você realmente é um gênio da Biologia!  \n" +
                    " Compartilhe essa conquista no Facebook.");
        }

        btnSair.setOnClickListener(v -> {
            Intent intent = new Intent(this, MainActivity.class);
            intent.setFlags( Intent.FLAG_ACTIVITY_CLEAR_TOP );
            this.startActivity(intent);
        });

        btnCategorias.setOnClickListener(v -> this.onBackPressed());

        btnShare.setOnClickListener(v -> {
            String quote = "Na categoria " + categoria + " acertei: " + hitsValue + ", com uma pontuação de " + scoreValue + " e minha acurácia foi de " + accuracyValue + "%";
            String contentTitle = getResources().getString(R.string.app_name) + " - " + categoria;

            ShareLinkContent content = new ShareLinkContent.Builder()
                    .setContentTitle(contentTitle)
                    .setContentDescription(txCongrats.getText().toString())
                    .setContentUrl(Uri.parse("https://dl.dropboxusercontent.com/u/36336684/ic_icon_biogame.png"))
                    .setQuote(quote)
                    .build();

            ShareDialog.show(this, content);
        });

        btnInvite.setOnClickListener(v -> {
            String appLinkUrl;

            appLinkUrl = "https://fb.me/259154904442220";

            if (AppInviteDialog.canShow()) {
                AppInviteContent content = new AppInviteContent.Builder()
                        .setApplinkUrl(appLinkUrl)
                        .build();
                AppInviteDialog.show(this, content);
            }
        });

    }
}
