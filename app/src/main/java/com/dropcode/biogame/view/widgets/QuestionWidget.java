package com.dropcode.biogame.view.widgets;

import android.content.Context;
import android.util.AttributeSet;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.dropcode.biogame.R;
import com.dropcode.biogame.model.Question;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by AlanmLira on 11/06/16.
 */
public class QuestionWidget extends LinearLayout {
    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.tx_category) public TextView txCategory;
    @BindView(R.id.tx_question) public TextView txQuestion;
    @BindView(R.id.tx_answer1)  public Button answer1;
    @BindView(R.id.tx_answer2)  public Button answer2;
    @BindView(R.id.tx_answer3)  public Button answer3;
    @BindView(R.id.tx_answer4)  public Button answer4;

    private Question mQuestion = new Question("", "", null, "");
    private List<Button> answerButtons = new ArrayList<>();

    public QuestionWidget(Context context) {
        super(context);
        inflate(context, R.layout.widget_question, this);

        initialize();
    }

    public QuestionWidget(Context context, AttributeSet attrs) {
        super(context, attrs);
        inflate(context, R.layout.widget_question, this);

        initialize();
    }

    private void initialize()
    {
        ButterKnife.bind(this);

        answerButtons.add(answer1);
        answerButtons.add(answer2);
        answerButtons.add(answer3);
        answerButtons.add(answer4);
    }

    public void setQuestion(Question q) {
        mQuestion = q;

        txQuestion.setText(q.question);
        txCategory.setText(q.category);

        q.allAnswers.add(q.rightAnswer);

        long seed = System.nanoTime();
        Collections.shuffle(q.allAnswers, new Random(seed));

        int indexQuestion = 0;
        for (String a: q.allAnswers)
        {
            switch (indexQuestion)
            {
                case 0:
                    answer1.setText(a);
                    break;
                case 1:
                    answer2.setText(a);
                    break;
                case 2:
                    answer3.setText(a);
                    break;
                case 3:
                    answer4.setText(a);
                    break;
                default:
                    break;
            }
            indexQuestion++;
        }

        answerRequest(new Button(getContext()));
    }

    public Boolean answerRequest(Button aButton)
    {
        Boolean result = false;
        for (Button b: answerButtons)
        {
            if (b.equals(aButton))
            {
                if (aButton.getText().toString().equals(mQuestion.rightAnswer))
                {
                    b.setBackgroundResource(R.drawable.green_corner_background);
                    result = true;
                }
                else
                {
                    b.setBackgroundResource(R.drawable.red_corner_background);
                }
                b.setTextColor(getResources().getColor(R.color.textIcons));
            }
            else
            {
                b.setBackgroundResource(R.drawable.hightlight_button);
                b.setTextColor(getResources().getColor(R.color.primaryText));
            }
        }
        return result;
    }

}
