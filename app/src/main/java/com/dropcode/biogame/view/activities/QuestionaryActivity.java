package com.dropcode.biogame.view.activities;

import android.animation.ObjectAnimator;
import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.animation.LinearInterpolator;
import android.widget.Button;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.dropcode.biogame.R;
import com.dropcode.biogame.api.BioGameApi;
import com.dropcode.biogame.infrastructure.di.qualifier.Background;
import com.dropcode.biogame.model.Question;
import com.dropcode.biogame.view.widgets.QuestionWidget;
import com.jakewharton.rxbinding.view.RxView;

import java.util.Collections;
import java.util.List;
import java.util.Random;

import javax.inject.Inject;

import butterknife.BindView;
import butterknife.ButterKnife;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by alan on 6/13/16.
 */
public class QuestionaryActivity extends BaseActivity {
    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.frame_question) QuestionWidget questionWidget;
    @BindView(R.id.tx_count) TextView txCount;
    @BindView(R.id.progress) ProgressBar progressBar;

    @Inject BioGameApi bioGameApi;

    @Inject @Background Scheduler backgroundScheduler;

    private String typeCategory = "";
    private String mCategoria = "Todos";

    private ProgressDialog progressDialog;
    private List<Question> mQuestions;
    private int mIndexQuestion = 0;
    private ObjectAnimator mProgressAnimator;

    private AlertDialog alertaCustom;

    private int hits = 0;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_questionary);
        getApplicationComponent().inject(this);

        ButterKnife.bind(this);

        mProgressAnimator = ObjectAnimator.ofInt(progressBar, "progress", 0, 0);
        mProgressAnimator.setDuration(300);
        mProgressAnimator.setInterpolator(new LinearInterpolator());
        mProgressAnimator.start();

        typeCategory = getIntent().getStringExtra(getString(R.string.category));

        requestQuestions();

        RxView.clicks(questionWidget.answer1)
                .compose(bindToLifecycle())
                .subscribe(aVoid -> clickAnswer(questionWidget.answer1));

        RxView.clicks(questionWidget.answer2)
                .compose(bindToLifecycle())
                .subscribe(aVoid -> clickAnswer(questionWidget.answer2));

        RxView.clicks(questionWidget.answer3)
                .compose(bindToLifecycle())
                .subscribe(aVoid -> clickAnswer(questionWidget.answer3));

        RxView.clicks(questionWidget.answer4)
                .compose(bindToLifecycle())
                .subscribe(aVoid -> clickAnswer(questionWidget.answer4));
    }

    private void requestQuestions() {
        if (typeCategory.equalsIgnoreCase("evolucao")) {
            mCategoria = "Evolução";
            bioGameApi.requestEvolutionQuests()
                    .subscribeOn(backgroundScheduler)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(this::showLoading)
                    .doOnCompleted(this::hideLoading)
                    .doOnError(t -> hideLoading())
                    .subscribe(this::showQuestions, this::handleError);


        } else if (typeCategory.equalsIgnoreCase("genetica")) {
            mCategoria = "Genética";
            bioGameApi.requestGeneticQuests()
                    .subscribeOn(backgroundScheduler)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(this::showLoading)
                    .doOnCompleted(this::hideLoading)
                    .doOnError(t -> hideLoading())
                    .subscribe(this::showQuestions, this::handleError);



        } else if (typeCategory.equalsIgnoreCase("microbiologia")) {
            mCategoria = "Microbiologia";
            bioGameApi.requestMicrobiologyQuests()
                    .subscribeOn(backgroundScheduler)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(this::showLoading)
                    .doOnCompleted(this::hideLoading)
                    .doOnError(t -> hideLoading())
                    .subscribe(this::showQuestions, this::handleError);



        } else if (typeCategory.equalsIgnoreCase("parasitologia")) {
            mCategoria = "Parasitologia";
            bioGameApi.requestParasitologyQuests()
                    .subscribeOn(backgroundScheduler)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(this::showLoading)
                    .doOnCompleted(this::hideLoading)
                    .doOnError(t -> hideLoading())
                    .subscribe(this::showQuestions, this::handleError);


        } else if (typeCategory.equalsIgnoreCase("zoologia")) {
            mCategoria = "Zoologia";
            bioGameApi.requestZoologyQuests()
                    .subscribeOn(backgroundScheduler)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(this::showLoading)
                    .doOnCompleted(this::hideLoading)
                    .doOnError(t -> hideLoading())
                    .subscribe(this::showQuestions, this::handleError);



        } else {
            mCategoria = "Todos";

            bioGameApi
                    .requestAllQuests()
                    .subscribeOn(backgroundScheduler)
                    .observeOn(AndroidSchedulers.mainThread())
                    .doOnSubscribe(this::showLoading)
                    .doOnCompleted(this::hideLoading)
                    .doOnError(t -> hideLoading())
                    .subscribe(this::showQuestions, this::handleError);
        }

    }

    private void showQuestions(List<Question> questions) {
        mQuestions = questions;

        hideLoading();

        long seed = System.nanoTime();
        Collections.shuffle(mQuestions, new Random(seed));
        changeQuest(mIndexQuestion - 1);

        changeCountText();
    }

    private void changeQuest() {
        changeQuest(mIndexQuestion);
    }

    private void changeQuest(int index) {
        hideCustomLoading();
        if (mIndexQuestion >= mQuestions.size() - 1) {

            String hits = String.valueOf(this.hits) + "/" + mQuestions.size();
            String accuracy = String.valueOf((int)((double)this.hits / (double)mQuestions.size() * 100));
            String score = String.valueOf(this.hits * 100);


            Intent con = new Intent(this, CongratulationsActivity.class);
            con.putExtra("hits", hits);
            con.putExtra("accuracy", accuracy);
            con.putExtra("score", score);
            con.putExtra("category", mCategoria);
            startActivity(con);
            finish();

            return;
        }
        mIndexQuestion = index + 1;
        questionWidget.setQuestion(mQuestions.get(mIndexQuestion));

        changeCountText();
    }

    private void handleError(Throwable throwable) {
        Log.e("Retrofit", throwable.getMessage(), throwable);
        Snackbar.make(questionWidget, "Erro ao carregar informações do servidor: " + throwable.getMessage() + "! Por favor, tente novamente!", Snackbar.LENGTH_LONG ).show();

        finish();
    }

    private void showMidLoading()
    {
        progressDialog = ProgressDialog.show(this, "", "Carregando...");
    }

    private void showCustomMidLoading(Boolean success)
    {
        LayoutInflater li = getLayoutInflater();
        int resource = success ? R.layout.dialog_success : R.layout.dialog_fail;

        View view = li.inflate(resource, null);
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setView(view);
        alertaCustom = builder.create();
        alertaCustom.show();
    }

    private void hideCustomLoading()
    {
        if (alertaCustom != null && alertaCustom.isShowing()) {
            alertaCustom.dismiss();
        }
    }

    private void showLoading()
    {
        progressDialog = ProgressDialog.show(this, "Aguarde", "Carregando...");
    }

    private void hideLoading()
    {
        if (progressDialog != null && progressDialog.isShowing()) {
            progressDialog.dismiss();
        }

        if (alertaCustom != null && alertaCustom.isShowing()) {
            alertaCustom.dismiss();
        }
    }

    public void clickAnswer(Button button) {

        if (questionWidget.answerRequest(button))
        {
            showCustomMidLoading(true);
            hits ++;
        } else {
            showCustomMidLoading(false);
        }

        int progress = (int)((double) (mIndexQuestion + 1) / mQuestions.size() * 100);
        mProgressAnimator = ObjectAnimator.ofInt(progressBar, "progress", progressBar.getProgress(), progress);
        mProgressAnimator.start();

        changeCountText();

        new Handler().postDelayed(this::changeQuest, 700);

    }

    private void changeCountText()
    {
        int size = mQuestions.size();
        txCount.setText((mIndexQuestion + 1) + " / " + size);
    }

    @Override
    public void onBackPressed() {

        new AlertDialog.Builder(this)
                .setTitle("Atenção")
                .setMessage("Deseja realmente cancelar seu progresso de jogo?")
                .setPositiveButton("Sim", (arg0, arg1) -> {
                    QuestionaryActivity.super.onBackPressed();
                })
                .setNegativeButton("Não", (arg0, arg1) -> {
                    Log.i(TAG, "Alert cancelado.");
                })
                .create()
                .show();
    }
}
