package com.dropcode.biogame.view.activities;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;

import com.bumptech.glide.Glide;
import com.dropcode.biogame.R;
import com.dropcode.biogame.model.User;
import com.dropcode.biogame.utils.Preferences;
import com.facebook.AccessToken;
import com.facebook.AccessTokenTracker;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.login.LoginManager;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;
import com.facebook.share.model.ShareLinkContent;
import com.facebook.share.widget.ShareDialog;

import org.json.JSONException;

import butterknife.BindView;
import butterknife.ButterKnife;

public class HomeActivity extends BaseActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.toolbar) Toolbar toolbar;
    @BindView(R.id.drawer_layout) DrawerLayout drawer;
    @BindView(R.id.nav_view) NavigationView navigationView;
    @BindView(R.id.login_button) LoginButton loginButton;
    @BindView(R.id.btn_todos) Button todosButton;
    @BindView(R.id.btn_parasitas) Button parasitasButton;
    @BindView(R.id.btn_evolucao) Button evolucaoButton;
    @BindView(R.id.btn_genetica) Button geneticaButton;
    @BindView(R.id.btn_microbiologia) Button microButton;
    @BindView(R.id.btn_zoologia) Button zooButton;


    private TextView txName;
    private TextView txEmail;
    private ImageView imgPhoto;

    private CallbackManager callbackManager;
    private AccessTokenTracker accessTokenTracker;

    private User user;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);

        initializeViews();
    }

    private void initializeViews()
    {

        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar,
                R.string.navigation_drawer_open,
                R.string.navigation_drawer_close);

        drawer.setDrawerListener(toggle);
        toggle.syncState();

        navigationView.setNavigationItemSelectedListener(this);

        View headerView = navigationView.inflateHeaderView(R.layout.nav_header_home);
        txEmail = (TextView) headerView.findViewById(R.id.tx_email);
        txName = (TextView) headerView.findViewById(R.id.tx_name);
        imgPhoto = (ImageView) headerView.findViewById(R.id.img_photo);

        populateHeaderDrawer(true);

        parasitasButton.setOnClickListener(v -> {
            Intent questionary = new Intent(this, QuestionaryActivity.class);
            questionary.putExtra("category","parasitologia");
            startActivity(questionary);
        });

        zooButton.setOnClickListener(v -> {
            Intent questionary = new Intent(this, QuestionaryActivity.class);
            questionary.putExtra("category","zoologia");
            startActivity(questionary);
        });

        geneticaButton.setOnClickListener(v -> {
            Intent questionary = new Intent(this, QuestionaryActivity.class);
            questionary.putExtra("category","genetica");
            startActivity(questionary);
        });

        microButton.setOnClickListener(v -> {
            Intent questionary = new Intent(this, QuestionaryActivity.class);
            questionary.putExtra("category","microbiologia");
            startActivity(questionary);
        });

        evolucaoButton.setOnClickListener(v -> {
            Intent questionary = new Intent(this, QuestionaryActivity.class);
            questionary.putExtra("category","evolucao");
            startActivity(questionary);
        });

        todosButton.setOnClickListener(v -> {
            Intent questionary = new Intent(this, QuestionaryActivity.class);
            questionary.putExtra("category","all");
            startActivity(questionary);
        });

        FacebookSdk.sdkInitialize(getApplicationContext());

        callbackManager = CallbackManager.Factory.create();

        LoginManager.getInstance().registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        Log.i(TAG, loginResult.toString());
                    }

                    @Override
                    public void onCancel() {
                        // App code
                        Log.i(TAG, "Cancelado");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        // App code
                        Log.e(TAG, exception.getMessage());
                    }
                });

        loginButton.setReadPermissions("email", "public_profile");

        accessTokenTracker = new AccessTokenTracker() {
            @Override
            protected void onCurrentAccessTokenChanged(
                    AccessToken oldAccessToken,
                    AccessToken currentAccessToken) {
                Log.e(TAG, "oldAccessToken: " + oldAccessToken + " - currentAccessToken: " + currentAccessToken);
                // Set the access token using
                // currentAccessToken when it's loaded or set.
                if (null == currentAccessToken) {
                    Preferences.deleteUserInPreferences(HomeActivity.this);

                    Intent main = new Intent(HomeActivity.this, MainActivity.class);
                    startActivity(main);
                }
            }
        };

        loginButton.registerCallback(callbackManager, new FacebookCallback<LoginResult>() {
            @Override
            public void onSuccess(LoginResult loginResult) {
                Log.i(TAG, loginResult.toString());

                final String accessToken = loginResult.getAccessToken().getToken();

                GraphRequest request = GraphRequest.newMeRequest( loginResult.getAccessToken(),
                        (object, response) -> {
                            Log.v("LoginActivity", response.toString());
                            // Application code String email = object.getString("email");
                            // String birthday = object.getString("birthday"); // 01/31/1980 format
                            user = new User();
                            try {
                                int id = 0;
                                String name = "";
                                String email = "";
                                String photoUrl = "";

                                if (object.has("id"))
                                    id = object.getInt("id");
                                if (object.has("name"))
                                    name = object.getString("name");
                                if (object.has("email"))
                                    email = object.getString("email");
                                if (object.has("picture"))
                                    photoUrl = object.getJSONObject("picture").getJSONObject("data").getString("url");


                                user = new User(
                                        id, name,
                                        email, accessToken,
                                        photoUrl);
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }

                            populateHeaderDrawer(false);

                            Preferences.saveUserInPreferences(HomeActivity.this, user);

                        });

                Bundle parameters = new Bundle();
                parameters.putString("fields", "id,name,email,gender,birthday,picture.type(large)");
                request.setParameters(parameters);
                request.executeAsync();
            }

            @Override
            public void onCancel() {
                Log.i(TAG, "Cancelado");
            }

            @Override
            public void onError(FacebookException error) {
                Log.e(TAG, error.getMessage());
            }
        });
    }


    private void populateHeaderDrawer(Boolean creation) {
        if (creation) {
            user = Preferences.getUserInPreferences(HomeActivity.this);
        }

        if (null != user.email && !user.email.isEmpty())
        {
            txEmail.setText(user.email);
        }
        if (null != user.name && !user.name.isEmpty())
        {
            txName.setText(user.name);
        }

        Log.i(TAG, "UserPhoto: " + user.photoUrl);

        if (!user.photoUrl.isEmpty())
        {
            Glide.with(HomeActivity.this)
                    .load(Uri.parse(user.photoUrl))
                    .fitCenter()
                    .crossFade()
                    .into(imgPhoto);
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }

    @Override
    public void onBackPressed() {
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();

        accessTokenTracker.stopTracking();
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int id = item.getItemId();

        Intent questionary = new Intent(this, QuestionaryActivity.class);

        if (id == R.id.nav_evolucao) {new Intent(this, QuestionaryActivity.class);
            questionary.putExtra(getString(R.string.category),"evolucao");

        } else if (id == R.id.nav_genetica) {
            questionary.putExtra(getString(R.string.category),"genetica");

        } else if (id == R.id.nav_microbiologia) {
            questionary.putExtra(getString(R.string.category),"microbiologia");

        } else if (id == R.id.nav_parasito) {
            questionary.putExtra(getString(R.string.category),"parasitologia");

        } else if (id == R.id.nav_zoologia) {
            questionary.putExtra(getString(R.string.category),"zoologia");

        } else if (id == R.id.nav_all_bio) {
            questionary.putExtra(getString(R.string.category),"all");

        } else if (id == R.id.nav_share) {

            String contentTitle = getResources().getString(R.string.app_name);

            ShareLinkContent content = new ShareLinkContent.Builder()
                    .setContentTitle(contentTitle)
                    .setContentDescription(getResources().getString(R.string.slide3))
                    .setContentUrl(Uri.parse("https://dl.dropboxusercontent.com/u/36336684/ic_icon_biogame.png"))
                    .setQuote("Venha testar seus conhecimentos sobre biologia comigo.")
                    .build();

            ShareDialog.show(this, content);
        } else if (id == R.id.nav_send) {
            Intent intent = new Intent(this, AboutActivity.class);
            startActivity(intent);
        }

        if (questionary.hasExtra(getString(R.string.category))) {
            startActivity(questionary);
        }

        drawer.closeDrawer(GravityCompat.START);
        return true;
    }
}
