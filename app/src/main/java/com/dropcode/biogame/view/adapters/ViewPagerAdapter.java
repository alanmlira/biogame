package com.dropcode.biogame.view.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import com.dropcode.biogame.view.fragments.ViewPagerFragment;

/**
 * Created by AlanmLira on 14/06/16.
 */
public class ViewPagerAdapter extends FragmentStatePagerAdapter {

    public static final int NUM_PAGES = 5;

    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {
        ViewPagerFragment f = new ViewPagerFragment();
        f.position = position;

        return f;
    }

    @Override
    public int getCount() {
        return NUM_PAGES;
    }
}