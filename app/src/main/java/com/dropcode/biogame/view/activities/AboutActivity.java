package com.dropcode.biogame.view.activities;

import android.os.Bundle;
import android.support.v7.widget.Toolbar;

import com.dropcode.biogame.R;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by alan on 6/16/16.
 */
public class AboutActivity extends BaseActivity {
    private final String TAG = getClass().getSimpleName();

    @BindView(R.id.toolbar_about) Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_about);

        ButterKnife.bind(this);

        setSupportActionBar(toolbar);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
    }

}
