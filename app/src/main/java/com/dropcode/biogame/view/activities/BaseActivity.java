package com.dropcode.biogame.view.activities;

import android.content.Context;

import com.dropcode.biogame.BioGameApplication;
import com.dropcode.biogame.infrastructure.di.component.ApplicationComponent;
import com.trello.rxlifecycle.components.support.RxAppCompatActivity;

import uk.co.chrisjenx.calligraphy.CalligraphyContextWrapper;

/**
 * Created by AlanmLira on 11/06/16.
 */
public abstract class BaseActivity extends RxAppCompatActivity {
    public ApplicationComponent getApplicationComponent() {
        BioGameApplication rxBioGameApplication = (BioGameApplication) getApplication();
        return rxBioGameApplication.getApplicationComponent();
    }

    @Override
    protected void attachBaseContext(Context newBase) {
        super.attachBaseContext(CalligraphyContextWrapper.wrap(newBase));
    }
}
