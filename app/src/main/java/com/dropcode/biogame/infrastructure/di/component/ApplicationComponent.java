package com.dropcode.biogame.infrastructure.di.component;

import com.dropcode.biogame.infrastructure.di.module.ApplicationModule;
import com.dropcode.biogame.view.activities.MainActivity;
import com.dropcode.biogame.view.activities.QuestionaryActivity;

import javax.inject.Singleton;

import dagger.Component;

/**
 * Created by AlanmLira on 10/06/16.
 */
@Singleton
@Component( modules = { ApplicationModule.class } )
public interface ApplicationComponent {
    void inject(MainActivity mainActivity);
    void inject(QuestionaryActivity questionaryActivity);

}
