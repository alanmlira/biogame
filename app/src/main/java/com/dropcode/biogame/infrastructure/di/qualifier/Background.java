package com.dropcode.biogame.infrastructure.di.qualifier;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

import javax.inject.Qualifier;

/**
 * Created by AlanmLira on 10/06/16.
 */
@Qualifier
@Retention(RetentionPolicy.RUNTIME)
public @interface Background {
}
