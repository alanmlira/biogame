package com.dropcode.biogame.infrastructure.di.module;

import android.content.Context;
import android.content.res.Resources;
import android.os.HandlerThread;
import android.os.Looper;

import com.dropcode.biogame.BioGameApplication;
import com.dropcode.biogame.R;
import com.dropcode.biogame.api.BioGameApi;
import com.dropcode.biogame.infrastructure.di.qualifier.Background;
import com.dropcode.biogame.infrastructure.di.qualifier.BioGameApiHost;

import java.io.InputStream;

import dagger.Module;
import dagger.Provides;
import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Converter;
import retrofit2.Retrofit;
import retrofit2.adapter.rxjava.RxJavaCallAdapterFactory;
import retrofit2.converter.gson.GsonConverterFactory;
import rx.Scheduler;
import rx.android.schedulers.AndroidSchedulers;

/**
 * Created by AlanmLira on 10/06/16.
 */
@Module
public class ApplicationModule {
    private Context context;

    public ApplicationModule(Context context) {
        this.context = context;
    }

    @Provides
    public Context provideContext() {
        return context;
    }

    @Provides @Background public Scheduler provideBackgroundScheduler() {
        HandlerThread handlerThread = new HandlerThread("background-handler-thread");
        handlerThread.start();
        Looper looper = handlerThread.getLooper();
        return AndroidSchedulers.from(looper);
    }

    @Provides public HttpLoggingInterceptor provideHttpLoggingInterceptor() {
        final HttpLoggingInterceptor interceptor = new HttpLoggingInterceptor();
        interceptor.setLevel(HttpLoggingInterceptor.Level.BODY);
        return interceptor;
    }

    @Provides @BioGameApiHost public String provideMarvelApiHost() {
        return "https://dl.dropboxusercontent.com";
    }

    @Provides public OkHttpClient provideOkHttpClient(HttpLoggingInterceptor loggingInterceptor) {
        return new OkHttpClient.Builder()
                .addInterceptor(loggingInterceptor)
                .build();
    }

    @Provides public Converter.Factory provideConverterFactory() {
        return GsonConverterFactory.create();
    }

    @Provides public BioGameApi provideBioGameApi(@BioGameApiHost String omdbApiHost, OkHttpClient okHttpClient, Converter.Factory converterFactory) {
        return new Retrofit.Builder()
                .baseUrl(omdbApiHost)
                .addConverterFactory(converterFactory)
                .addCallAdapterFactory(RxJavaCallAdapterFactory.create())
                .client(okHttpClient)
                .build()
                .create(BioGameApi.class);
    }
}
