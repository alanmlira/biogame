package com.dropcode.biogame.infrastructure.di.module;

import android.content.res.Resources;
import android.net.Uri;

import com.dropcode.biogame.BioGameApplication;
import com.dropcode.biogame.R;
import com.dropcode.biogame.model.Question;
import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;

import java.io.IOException;
import java.io.InputStream;
import java.util.Collections;
import java.util.List;

import retrofit.client.Client;
import retrofit.client.Request;
import retrofit.client.Response;
import retrofit.mime.TypedByteArray;

/**
 * Created by AlanmLira on 16/06/16.
 */
public class MockClient implements Client {

    @Override
    public Response execute(Request request) throws IOException {
        Uri destination = Uri.parse(request.getUrl());
        String path = destination.getPath();

        String responseString = "";

        if (path.equalsIgnoreCase("/api/quests/evolution")) {
            responseString = getResourceAsString(R.raw.questions_evolucao);
        } else if (path.equalsIgnoreCase("/api/quests/genetica")) {
            responseString = getResourceAsString(R.raw.questions_genetica);
        } else if (path.equalsIgnoreCase("/api/quests/microbiologia")) {
            responseString = getResourceAsString(R.raw.questions_microbiologia);
        } else if (path.equalsIgnoreCase("/api/quests/parasitologia")) {
            responseString = getResourceAsString(R.raw.questions_parasitologia);
        } else if (path.equalsIgnoreCase("/api/quests/zoologia")) {
            responseString = getResourceAsString(R.raw.questions_zoologia);
        }

        return new Response(request.getUrl(), 200, "nothing", Collections.EMPTY_LIST, new TypedByteArray("application/json", responseString.getBytes()));
    }

    public List<Question> getEvolutionQuests()
    {

               String searchResult = getResourceAsString(R.raw.questions_evolucao);
               Gson gson = new Gson();
               return gson.fromJson(searchResult, new TypeToken<List<Question>>(){}.getType());
    }

    public List<Question> getGeneticQuests()
    {

        String searchResult = getResourceAsString(R.raw.questions_genetica);
        Gson gson = new Gson();
        return gson.fromJson(searchResult, new TypeToken<List<Question>>(){}.getType());
    }

    public List<Question> getMicroBiologyQuests()
    {

        String searchResult = getResourceAsString(R.raw.questions_microbiologia);
        Gson gson = new Gson();
        return gson.fromJson(searchResult, new TypeToken<List<Question>>(){}.getType());
    }

    public List<Question> getParasiteQuests()
    {

        String searchResult = getResourceAsString(R.raw.questions_parasitologia);
        Gson gson = new Gson();
        return gson.fromJson(searchResult, new TypeToken<List<Question>>(){}.getType());
    }

    public List<Question> getZooQuests()
    {

        String searchResult = getResourceAsString(R.raw.questions_zoologia);
        Gson gson = new Gson();
        return gson.fromJson(searchResult, new TypeToken<List<Question>>(){}.getType());
    }


    /**
     * Faz a leitura de um arquivo RAW de texto como String
     *
     * @param resourceId
     * @return
     */
    public String getResourceAsString(int resourceId) {
        try {
            Resources res = BioGameApplication.instance().getResources();
            InputStream in_s = res.openRawResource(resourceId);

            byte[] b = new byte[in_s.available()];
            in_s.read(b);

            return new String(b);
        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }
}
